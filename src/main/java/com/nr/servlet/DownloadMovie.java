package com.nr.servlet;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author Andrey Stepanov
 */
@WebServlet("/DownloadMovie/*")
@MultipartConfig
public class DownloadMovie extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public DownloadMovie() {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String restOfTheUrl = request.getRequestURI().substring(15);
        if (restOfTheUrl.length() == 0) {
            throw new IOException("Requested file not found");
        }
        File file = new File("/usr/local/moodme/video/outgoing/" + restOfTheUrl);
        response.setCharacterEncoding("UTF-8");
        final FileInputStream fileInputStream = new FileInputStream(file);

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "filename=" + restOfTheUrl);

        final ServletOutputStream outputStream = response.getOutputStream();
        IOUtils.copy(fileInputStream, outputStream);
        fileInputStream.close();
        outputStream.close();
    }


}
