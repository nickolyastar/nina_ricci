package com.nr.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Servlet implementation class TrasformMovie
 */
@WebServlet("/TrasformMovie")
@MultipartConfig
public class TrasformMovie extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TrasformMovie() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fileName = request.getParameter("video");
        String code = request.getParameter("code");
        String locale = request.getParameter("locale");

        final String moodMePath = "/usr/local/moodme";

        String path = moodMePath + "/video/incoming/";
        String outputPath = moodMePath + "/video/outgoing/";

        int masksCode = Integer.parseInt(new StringBuilder(code).reverse().toString(), 2);

        Process process = Runtime.getRuntime().exec(moodMePath + "/bin/process.sh " + path + File.separator + fileName + " " + outputPath + File.separator + fileName + " " + masksCode);
        try {
            process.waitFor();
        } catch (InterruptedException e) {
            throw new IOException(e);
        }
        response.sendRedirect(locale + "/step3.html?video=" + fileName);
    }
}
