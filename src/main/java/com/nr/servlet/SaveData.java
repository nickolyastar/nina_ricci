package com.nr.servlet;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;

/**
 * @author Andrey Stepanov
 */
@WebServlet("/SaveData/*")
public class SaveData extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public SaveData() {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String locale = request.getParameter("locale");

	String name = request.getParameter("name");
	String lastname = request.getParameter("lastname");
	String surname = request.getParameter("surname");
	String country = request.getParameter("countries");
	String email = request.getParameter("email");
	String agree = request.getParameter("agree");

        final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        final String DB_URL = "jdbc:mysql://localhost:3306/moodme";
        // ***********************************************************************
        // Database credentials
        final String USER = "moodme";
        final String PASS = "moodme";
        try {
		// Register JDBC driver
		Class.forName(JDBC_DRIVER);

		Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);

		PreparedStatement stmt = (PreparedStatement) conn.prepareStatement("insert into monsters_users (nation, name, surname, email, confirmed) values(?,?,?,?,?)");
		stmt.setString(1, country);
		stmt.setString(2, name);
		stmt.setString(3, lastname);
		stmt.setString(4, email);
		stmt.setBoolean(5, agree!=null);

		stmt.executeUpdate();
 
	} catch (Exception e) {
            e.printStackTrace();
        }

	response.sendRedirect(locale + "/index2.html");
    }


}
