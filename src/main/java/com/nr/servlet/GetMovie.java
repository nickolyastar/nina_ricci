package com.nr.servlet;


import com.nr.util.StreamingViewRenderer;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Andrey Stepanov
 */
@WebServlet("/GetMovie/*")
@MultipartConfig
public class GetMovie extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public GetMovie() {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String restOfTheUrl = request.getRequestURI().substring(10);
        if (restOfTheUrl.length() == 0) {
            throw new IOException("Requested file not found");
        }
        File file = new File("/usr/local/moodme/video/outgoing/" + restOfTheUrl);
        response.setCharacterEncoding("UTF-8");
        Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
        String contentType = "video/mp4";
        objectMap.put(StreamingViewRenderer.INPUT_STREAM, new FileInputStream(file));
        objectMap.put(StreamingViewRenderer.CONTENT_TYPE, contentType);
        objectMap.put(StreamingViewRenderer.CONTENT_LENGTH, file.length());
        objectMap.put(StreamingViewRenderer.FILENAME, URLEncoder.encode(file.getName(), "UTF-8"));
        objectMap.put(StreamingViewRenderer.LAST_MODIFIED, new Date(file.lastModified()));
        StreamingViewRenderer svr = new StreamingViewRenderer();
        try {
            svr.renderMergedOutputModel(objectMap, request, response);
        } catch (Exception e) {
            throw new IOException(e);
        }
    }


}
