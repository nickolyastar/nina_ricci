$(function(){




	$('#video').click( function(){

		if(!$(this).hasClass('play')){
			$('#video').get(0).play();
			$(this).addClass('play');
		}else{
			$('#video').get(0).pause();
			$(this).removeClass('play');
		}

	});



	if($('#countries').length){
		$("#countries").msDropdown({
			visibleRows: 5,
			rowHeight: 20
		});
	}


	if($('#modal-btn').length){
		setTimeout(function(){
			$("#modal-btn").fancybox().trigger('click');

		},5000);
	}

	$('#contact_form input').keyup(function(){
	    if ($('#contact_form').valid()){
    		$('#submit-btn').removeClass('btn-disabled');
	    } else{
	    	$('#submit-btn').addClass('btn-disabled');
	    }
	});

	$('#contact_form input[type="checkbox"]').change(function(){
	    if ($('#contact_form').valid()){
    		$('#submit-btn').removeClass('btn-disabled');
	    } else{
	    	$('#submit-btn').addClass('btn-disabled');
	    }
	});

	if($("#contact_form").length){
		$("#contact_form").validate({
		    errorPlacement: function(error, element) {
		      if (element.is(":checkbox") || element.is(":radio")) {
		        return false;
		      } else {
		        error.insertAfter(element);
		      }
		    }
		});
  	}






	if($('#terms-btn').length){

		$("#terms-btn").fancybox();

	}





	$('.masks input:checkbox').click(function(){

		if($(this).attr("name") == 'group1'){
			$('input[name="group2"]:checkbox').prop('checked', false);
		}else if($(this).attr("name") == 'group2'){
			$('input[name="group1"]:checkbox').prop('checked', false);
		}

		if($(this).attr("name") == 'group3'){
			$('input[name="group3"]:checkbox').not(this).prop('checked', false);
		}
	});


	$('#agree').change(function(event) {
		if($(this).is(":checked")){
			$('#agree_success').removeClass('btn-disabled');
		}else{
			$('#agree_success').addClass('btn-disabled');
		}
	});

	$('.lang span').click(function(){
		$(this).parent().addClass('hover');
		$('body').append('<div class="lang-overlay"></div>');
	});

	$('.lang a').click(function(event){
		$('.lang span').text($(this).text());
		$('.lang').removeClass('hover');
		$('.lang-overlay').remove();
	});

	$('body').on('click', '.lang-overlay',function(){
		$('.lang').removeClass('hover');
		$('.lang-overlay').remove();
	});










});
